#!/usr/bin/env bash
set -eu

############################################################
# Constants                                                #
############################################################

INSTALL_TARGET=/opt/innovativity/backup-cli
PATH_BIN_TARGET=/usr/local/bin
SYSTEMD_TARGET=/etc/systemd/system
SYSTEMD_SERVICE_NAME=backup-client.service

CONFIG_PATH=$INSTALL_TARGET/config
CONFIG_FILE=$CONFIG_PATH/config.yml
EXECUTABLE_PATH=$INSTALL_TARGET/bin/restic

# Command Options
OPTIONS_NOINPUT=
OPTIONS_DRYRUN=

# Setup Options
SETUP_BASE_URL=
SETUP_ACCESS_TOKEN=
SETUP_ALLOWED_PATHS=()


# Upgrade flags
IS_UPDATING=
SYSTEMD_SERVICE_STATUS=inactive


############################################################
# Help                                                     #
############################################################

function print_help() {

  # Display Help
  echo "Install Innovativity Backup Controller Client."
  echo
  echo "Syntax: $0 [-h|--help] [--dryrun|--dry-run] [--noinput|--no-input] [--base_url <SERVER-BASE-URL>] [--access-token] [...<BACKUP-PATHS>]"
  echo
  echo "options:"
  echo "-h|--help                Print this Help."
  echo "--noinput|--no-input     Never ask for user input."
  echo "--dryrun|--dry-run       Dry run (does not install the program)"
  echo "--base-url               Server Base URL."
  echo "--access-token           Client Auth Token."
  echo
}


############################################################
# Argument Parsing                                         #
############################################################

function parse_args() {
  while [[ $# -gt 0 ]]; do
    case $1 in
      -h|--help)
        print_help
        exit 0
        ;;
      --noinput|--no-input)
        OPTIONS_NOINPUT=1
        shift
        ;;
      --dryrun|--dry-run)
        OPTIONS_DRYRUN=1
        shift
        ;;
      --base-url)
        SETUP_BASE_URL="$2"
        shift # past argument
        shift # past value
        ;;
      --access-token)
        SETUP_ACCESS_TOKEN="$2"
        shift # past argument
        shift # past value
        ;;
      -*|--*)
        echo "Unknown option $1"
        print_help
        exit 1
        ;;
      *)
        SETUP_ALLOWED_PATHS+=("$1") # save positional arg
        shift # past argument
        ;;
    esac
  done

  # set -- "${SETUP_ALLOWED_PATHS[@]}" # restore positional parameters
}


############################################################
# Check Root Privileges                                    #
############################################################

function check_root() {
  if [ "$EUID" -ne 0 ]
    then echo "Please run as root"
    exit 1
  fi
}


############################################################
# Check previous versions / update                         #
############################################################

function check_update() {
  # ignore systemctl terminate code
  set +e
  SYSTEMD_SERVICE_STATUS=$(systemctl is-active $SYSTEMD_SERVICE_NAME)
  set -e
  if [ -f $CONFIG_FILE ]; then
    IS_UPDATING=1
  fi
}


############################################################
# Installation                                             #
############################################################

check_root
parse_args $@
check_update

if [ "$SYSTEMD_SERVICE_STATUS" == "active" ]; then
  echo "[INFO] Stopping running systemd service"
  if [ -z $OPTIONS_DRYRUN ]; then
    systemctl stop $SYSTEMD_SERVICE_NAME
  else
    echo "*SKIPPING: DRY RUN*"
  fi
fi

echo "[INFO] Installing into $INSTALL_TARGET"
if [ -z $OPTIONS_DRYRUN ]; then
  mkdir -p $INSTALL_TARGET
  cp -a . $INSTALL_TARGET
else
  echo "*SKIPPING: DRY RUN*"
fi

echo "[INFO] Adding executable symlink to $PATH_BIN_TARGET"
if [ -z $OPTIONS_DRYRUN ]; then
  rm -f $PATH_BIN_TARGET/backup-cli
  ln -s $INSTALL_TARGET/backup-cli $PATH_BIN_TARGET/backup-cli
else
  echo "*SKIPPING: DRY RUN*"
fi

echo "[INFO] Installing systemd service"
if [ -z $OPTIONS_DRYRUN ]; then
cat << EOF > $SYSTEMD_TARGET/$SYSTEMD_SERVICE_NAME
[Unit]
Description=Innovativity Backup Controller Client
After=network.target

[Service]
Type=simple
User=root
Group=root
Environment="BACKUP_CTRL_CONFIG_DIR=$CONFIG_PATH"
WorkingDirectory=$INSTALL_TARGET
ExecStart=$PATH_BIN_TARGET/backup-cli run
ExecReload=/bin/kill -s HUP \$MAINPID
KillMode=mixed
TimeoutStopSec=5
PrivateTmp=true
Restart=on-failure
StandardOutput=journal
StandardError=journal

[Install]
WantedBy=multi-user.target
EOF
systemctl daemon-reload
else
  echo "*SKIPPING: DRY RUN*"
fi

echo "[INFO] Checking client configuration"

# if it's not updating, check for config variables and request them interactively if --noinput is not set
if [ -z $IS_UPDATING ] && ([-z $SETUP_BASE_URL ] || [ -z $SETUP_ACCESS_TOKEN ]); then
  if [ -z $OPTIONS_NOINPUT ]; then
    read -p "Missing client configuration options. Do you want to configure it now? [y|N]" configure_now
    if [ "$configure_now" == "y" ] || [ "$configure_now" == "Y" ]; then
      while [ -z $SETUP_BASE_URL ]; do
        read -p "Server Base URL [ws://example.com/]: " SETUP_BASE_URL
      done
      while [ -z $SETUP_ACCESS_TOKEN ]; do
        read -p "Client Access Token [hidden]: " -s SETUP_ACCESS_TOKEN
        echo
      done
    fi
  else
    echo "[WARNING] Skipping client configuration: --base-url and --access-token must both be defined if running with --noinput"
  fi
fi


if [ $SETUP_BASE_URL ] && [ $SETUP_ACCESS_TOKEN ]; then
  echo "[INFO] Configuring the client"
  if [ -z $OPTIONS_DRYRUN ]; then
    backup-cli config backend restic
    backup-cli config client websocket --base-url $SETUP_BASE_URL --access-token $SETUP_ACCESS_TOKEN
    backup-cli config paths ${SETUP_ALLOWED_PATHS[@]}
    SYSTEMD_SERVICE_STATUS=active
  else
    echo "*SKIPPING: DRY RUN*"
  fi
else
  if [ -z $IS_UPDATING ]; then
    # configure the backend anyway
    backup-cli config backend restic
    echo
    echo "**********************************************************************"
    echo "[NOTE] Skipping client configuration"
    echo "To configure the client by yourself, run these configuration commands:"
    echo "  $ backup-cli config client websocket -h"
    echo "  $ backup-cli config paths -h"
    echo "Then enable the system service using systemctl:"
    echo "  $ systemctl enable $SYSTEMD_SERVICE_NAME"
    echo "**********************************************************************"
  fi
fi


if [ $SYSTEMD_SERVICE_STATUS == "active" ]; then
  echo "[INFO] Starting systemd service"
  if [ -z $OPTIONS_DRYRUN ]; then
    systemctl enable $SYSTEMD_SERVICE_NAME
    systemctl start $SYSTEMD_SERVICE_NAME
  else
    echo "*SKIPPING: DRY RUN*"
  fi
fi

if [ -z $IS_UPDATING ]; then
  echo
  echo "##########################"
  echo "# Installation completed #"
  echo "##########################"
  echo "Run 'backup-cli' to get started."
  echo "Check the service status with 'systemctl status $SYSTEMD_SERVICE_NAME'."
else
  echo
  echo "####################"
  echo "# Update completed #"
  echo "####################"
  echo "Run 'backup-cli' to get started."
  echo "Check the service status with 'systemctl status $SYSTEMD_SERVICE_NAME'."
fi
