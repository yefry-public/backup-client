# linux amd64 build installer

RESTIC_VERSION=0.14.0

echo "Setting up directory"
mkdir tmp
cp setup/entrypoints/backup-client-linux.py backup-client.py
cp setup/installers/linux-amd64.sh setup.sh

echo "Downloading restic"
curl -L "https://github.com/restic/restic/releases/download/v${RESTIC_VERSION}/restic_${RESTIC_VERSION}_linux_amd64.bz2" | bzip2 -d > tmp/restic

echo "Compiling"
pyinstaller backup-client.py -n backup-cli --add-data="setup.sh:." --add-data="tmp/restic:bin/" --noconfirm

echo "Packaging"
makeself --needroot dist/backup-cli dist/backup-client-installer.run InnovativityBackupClient ./setup.sh

echo "Cleaning Up"
rm backup-client.py
rm setup.sh
rm backup-cli.spec
rm -rf tmp

echo "Build Done"
