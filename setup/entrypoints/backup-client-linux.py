"""
Backup Controller - Compiled Entrypoint

This is the entrypoint for the compiled distribution of this software.
The installation is expected to be done in /opt/innovativity/backup-cli and all configuration files
will follow suit in that directory.

The installation is system-wide and executed as a systemd service with root user.
"""
import logging
import os

if __name__ == "__main__":
    os.environ.setdefault("BACKUP_CTRL_CONFIG_DIR", "/opt/innovativity/backup-cli/config")
    os.environ.setdefault("BACKUP_CTRL_RESTIC_PATH", "/opt/innovativity/backup-cli/bin/restic")

    from backup_controller.cli import main
    from backup_controller.conf import setup_logging, setup_signals
    from backup_controller.utils import terminate

    # noinspection PyBroadException
    try:
        setup_logging()
    except Exception:
        logging.error(f"Logging setup failed, continuing without it", exc_info=True)

    # noinspection PyBroadException
    try:
        setup_signals()
    except Exception:
        logging.error(f"Signal handlers setup failed, continuing without them", exc_info=True)

    try:
        # noinspection PyUnresolvedReferences,PyProtectedMember
        terminate(main())
    except Exception as e:
        logging.critical(e, exc_info=True)
        # noinspection PyUnresolvedReferences,PyProtectedMember
        terminate(255)
