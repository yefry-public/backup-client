pydantic==1.8.2
schedule==1.1.0
PyYAML==6.0
websocket-client==1.2.1
pytz==2021.3
certifi==2021.10.8
