import logging
import threading
import time
from typing import Optional

from . import schedule

cease_continuous_run: Optional[threading.Event] = None
continuous_thread: Optional["ScheduleThread"] = None

logger = logging.getLogger(__name__)


class ScheduleThread(threading.Thread):
    @classmethod
    def run(cls):
        while not cease_continuous_run.is_set():
            try:
                schedule.run_pending()
            except Exception as e:
                logger.error(e, exc_info=True)
                cease_continuous_run.set()

            time.sleep(1)


def is_running():
    if cease_continuous_run is None or cease_continuous_run.is_set():
        return False
    return True


def run_threaded(job_func):
    job_thread = threading.Thread(target=job_func)
    job_thread.start()


def start() -> threading.Event:
    """
    Continuously run while executing pending jobs.

    :return: event to stop execution
    """
    global cease_continuous_run
    global continuous_thread

    if cease_continuous_run is not None and not cease_continuous_run.is_set():
        raise AttributeError("Already Running")

    cease_continuous_run = threading.Event()
    continuous_thread = ScheduleThread()
    continuous_thread.start()
    return cease_continuous_run


def stop():
    cease_continuous_run.set()
