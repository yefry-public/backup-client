import argparse

from backup_controller import __version__

from backup_controller.cli import commands


def parse_args():
    """
    Parse arguments using ``argparse`` and registered :ref:`~backup_controller.cli.commands.abstract.Command` classes.

    :return: Parsed arguments
    """
    parser = argparse.ArgumentParser(
        prog="backup-cli",
        description=f"Innovativity Backup Controller CLI version {__version__}"
    )

    # version action: prints the version then exits
    # https://docs.python.org/3/library/argparse.html#action
    parser.add_argument("-v", "--version", action="version", version=__version__)

    # print help if no argument is set
    parser.set_defaults(handler=lambda *a: parser.print_help())

    # --- global options ---
    # --- end global options ---

    # --- commands ---
    subparsers = parser.add_subparsers(title="Commands", help="Action")

    for command_class in commands.available_commands:
        sub_parser = subparsers.add_parser(command_class.name, help=command_class.help)
        command = command_class(sub_parser)
        sub_parser.set_defaults(handler=command)
    # --- end commands ---

    return parser.parse_args()


def main() -> int:
    args = parse_args()
    # default to exit code 0 if handler doesn't return anything
    return args.handler(args) or 0
