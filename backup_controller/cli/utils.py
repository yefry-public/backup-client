"""
Utilities for the CLI package.
"""
from argparse import ArgumentParser
from typing import Union, Type, Optional, Iterable

from pydantic import BaseModel


def add_arguments_from_model(
    parser: ArgumentParser,
    model: Union[BaseModel, Type[BaseModel]],
    positional: bool = False,
    prefix: str = "",
    replace_underscores: bool = True,
    fields: Optional[Iterable[str]] = None,
    exclude_fields: Optional[Iterable[str]] = None,
):
    """
    Add arguments to parser based on a pydantic Model.

    :param parser: Argument Parser to update
    :param model: Model to get fields from
    :param positional: Set to True to use positional arguments. Default False
    :param prefix: Add a prefix to the field names
    :param replace_underscores: Replace underscores with dashes
    :param fields: Limit fields to the listed ones. Invalid fields will be ignored. If empty, all model fields will
    be added
    :param exclude_fields: Fields contained in this list will be excluded
    """
    if not positional:
        prefix = f"--{prefix}"

    for field_name, field in model.__fields__.items():
        if fields and field_name not in fields:
            continue
        if exclude_fields and field_name in exclude_fields:
            continue
        if replace_underscores:
            field_name = field_name.replace('_', '-')
        parser.add_argument(f"{prefix}{field_name}", nargs="?", type=field.type_, required=field.required)
