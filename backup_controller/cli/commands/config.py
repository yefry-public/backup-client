from pathlib import Path

from backup_controller.cli.utils import add_arguments_from_model
from backup_controller.utils import as_json
from backup_controller.conf import config
from backup_controller.models import (
    RetentionPolicy,
    BackupPolicy,
    Location,
)
from .abstract import Command, CommandError

__all__ = ("ConfigCommand",)


class ConfigBackupPolicyAddSubCommand(Command):
    name = "add"
    help = "Add Backup Policy"

    def add_arguments(self, parser):
        # parser.add_argument("action", choices=["add", "ls", "rm"])
        parser.add_argument("delay_minutes", type=int, nargs=1)
        parser.add_argument("paths", type=Path, nargs="+")
        add_arguments_from_model(parser, RetentionPolicy, prefix="retention-", exclude_fields=["keep_years"])

    def handle(self, args):
        policy = BackupPolicy(
            paths=args.paths,
            delay_minutes=args.delay_minutes[0],
            locations=[
                # todo hardcoded
                Location(
                    bucket_url="http://localhost:9000/backup",
                    access_key_id="backup",
                    secret_access_key="password",
                    retention_policy=RetentionPolicy(
                        keep_last=args.retention_keep_last,
                    ),
                )
            ],
        )
        config.backup_policies.append(policy)
        config.save()


class ConfigBackupPolicySubCommand(Command):
    name = "backup"
    help = "Update Backup Policy"
    subcommands = []

    def handle(self, args):
        for policy in config.backup_policies:
            self.print(policy, stdout=True)


class ConfigPathsPolicySubCommand(Command):
    name = "paths"
    help = "Change Allowed Backup Paths"

    def add_arguments(self, parser):
        parser.add_argument("paths", nargs="*", help="Paths allowed for backup")
        parser.add_argument("--json", action="store_true", help="Output as json")

    def handle(self, args):
        if args.paths:
            config.allowed_paths = args.paths
            try:
                config.sanity_check()
            except ValueError as e:
                # todo: this should be allowed, improve
                self.print(
                    f"An error occurred while saving the configuration. "
                    f"You might be removing a previously allowed path which is still in use."
                )
                raise CommandError(f"Configuration error: {e}", print_help=False)

            config.save()
            self.print(f"Configuration updated")

        if args.json:
            self.print(as_json(config.allowed_paths), stdout=True)
        else:
            print_paths = "\n - ".join(str(path) for path in config.allowed_paths)
            self.print(f"Paths allowed for backup:\n - {print_paths}", stdout=True)


class ConfigClientWebsocketSubCommand(Command):
    name = "websocket"
    help = "Set Websocket Client Configuration"

    def add_arguments(self, parser):
        parser.add_argument("--base-url", type=str)
        parser.add_argument("--access-token", type=str)

    def handle(self, args):
        from backup_controller.conf.models import WebsocketClientConfig

        args_values = (args.base_url, args.access_token)

        if isinstance(config.client_config, WebsocketClientConfig):
            if not any(args_values):
                raise CommandError("No change requested")

            if args.base_url:
                config.client_config.base_url = args.base_url
                self.print(f"Changed Websocket Client Base URL to '{args.base_url}'")
            if args.access_token:
                config.client_config.access_token = args.access_token
                self.print(f"Changed Websocket Client Access Token")
        else:
            if not all(args_values):
                raise CommandError(
                    "Both '--base-url' and '--access-token' must be specified to change Client to Websocket."
                )

            config.client_config = WebsocketClientConfig(base_url=args.base_url, access_token=args.access_token)
            self.print(f"Changed Client to Websocket Client with Base URL '{args.base_url}'")

        config.save()
        self.print(f"Configuration updated")


class ConfigClientSubCommand(Command):
    name = "client"
    help = "Set Client Configuration"
    subcommands = [ConfigClientWebsocketSubCommand]

    def handle(self, args):
        raise CommandError("You must specify a client type.")


class ConfigBackupBackendSubCommand(Command):
    name = "backend"
    help = "Set Backup Backend Configuration"

    def add_arguments(self, parser):
        parser.add_argument("backend", choices=["restic", "dummy"])

    def handle(self, args):
        from backup_controller.conf import models

        client_configs = {
            "restic": models.ResticBackendConfig(),
            "dummy": models.DummyBackendConfig(),
        }
        config.backend_config = client_configs[args.backend]
        config.save()


class ConfigCommand(Command):
    name = "config"
    help = "Update Configuration"
    subcommands = (
        ConfigClientSubCommand,
        ConfigBackupBackendSubCommand,
        ConfigBackupPolicySubCommand,
        ConfigPathsPolicySubCommand,
    )

    def add_arguments(self, parser):
        pass

    def handle(self, args):
        raise CommandError("You must specify a sub action.")
