import logging

from .abstract import Command
from backup_controller.conf import config


class RunDaemonCommand(Command):
    name = "run"
    help = "Run the backup Daemon"

    def handle(self, args):
        client = config.client_config.get_client()
        try:
            client.run()
        except KeyboardInterrupt:
            logging.debug("Keyboard Interrupt")
            client.stop()

