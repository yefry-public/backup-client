import abc
import sys
from argparse import ArgumentParser, Namespace
from enum import Enum
from typing import Iterable, Type, Optional


class ExitStatus(int, Enum):
    """
    Exit Status Codes
    """

    OK = 0
    GENERIC_ERROR = -1
    COMMAND_ERROR = 1


class CommandError(Exception):
    """
    Base Exception for Command Errors.

    :arg msg: Error Message
    :arg exit_status: Exit Status
    :arg print_help: print command help on terminate
    """

    def __init__(self, msg, exit_status: ExitStatus = ExitStatus.COMMAND_ERROR, print_help: bool = True):
        self.exit_status = exit_status
        self.print_help = print_help
        super(CommandError, self).__init__(msg)


class Command(abc.ABC):
    """
    Base Command

    Extend this class to create a CLI command.

    :param parser: Argparse Parser
    """

    name: str
    help: str
    subcommands: Iterable[Type["Command"]] = ()

    CommandError = CommandError

    def __init__(self, parser: ArgumentParser, parent: Optional["Command"] = None):
        self.parser = parser
        self.parent = parent

        if self.subcommands:
            subparsers = parser.add_subparsers()

            for command_class in self.subcommands:
                sub_parser = subparsers.add_parser(command_class.name, help=command_class.help)
                command = command_class(sub_parser, parent=self)
                sub_parser.set_defaults(handler=command)

        self.add_arguments(parser)

    def __call__(self, args: Namespace):
        try:
            self.handle(args)
        except CommandError as e:
            self.print(e)
            if e.print_help:
                self.parser.print_help(file=sys.stderr)
            return e.exit_status

        return ExitStatus.OK

    def print(self, *values, stdout=False, **kwargs):
        """
        Print a message to console via stderr.

        :param values: Message to print
        :param stdout: If True, prints to standard output instead of standard error
        :param kwargs: Kwargs for ``print``
        """
        if stdout:
            stream = sys.stdout
        else:
            stream = sys.stderr

        print(*values, file=stream, **kwargs)

    def add_arguments(self, parser: ArgumentParser):
        """Override this method to add parser arguments to the command."""

    @abc.abstractmethod
    def handle(self, args: Namespace):
        """Override this method to handle the command."""
