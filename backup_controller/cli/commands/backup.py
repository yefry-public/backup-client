import logging
import uuid

from backup_controller.conf import config
from backup_controller.models import BackupNowAction, RestoreBackupAction, GetSnapshotContentAction
from backup_controller.utils import filter_objects
from .abstract import Command


class ListBackupSnapshotsCommand(Command):
    name = "ls"
    help = "List Backup Snapshot (or snapshot files)"

    def add_arguments(self, parser):
        parser.add_argument(
            "snapshot_id",
            nargs="?",
            help="list contents of this specific snapshot. Leave empty to get a list of all snapshots instead",
        )

    def print_node(self, node, prefix=""):
        if node.is_dir:
            print(prefix + node.name)
            for node_content in node.content:
                self.print_node(node_content, prefix=prefix + " ")
        else:
            print(prefix + node.name)

    def handle(self, args):
        locations = []
        for policy in config.backup_policies:
            locations += policy.locations

        # remove duplicated locations
        locations = set(locations)

        if args.snapshot_id:
            for location in locations:
                print(f"Snapshot {args.snapshot_id} (location {location.bucket_url}) contents:")
                for node in (
                    GetSnapshotContentAction(id=f"CLI-{uuid.uuid4()}", location=location, snapshot_id=args.snapshot_id)
                    .execute()
                    .content
                ):
                    self.print_node(node)
        else:
            for location in locations:
                print(f"Snapshots for location {location.bucket_url}:")
                for snapshot in config.backend.list(location):
                    print(snapshot.time, snapshot.id)


class BackupRestoreCommand(Command):
    name = "restore"
    help = "Restore Backup"

    def add_arguments(self, parser):
        parser.add_argument(
            "snapshot_id",
            help="snapshot to restore. Run the `ls` subcommand to get a list of available snapshots",
        )
        parser.add_argument(
            "path",
            nargs="*",
            help="restore only these files/paths (leave empty to restore everything or use the --exclude option)",
        )
        parser.add_argument(
            "-t",
            "--target",
            nargs="?",
            default="/",
            help="restore the snapshot in this target directory",
        )
        parser.add_argument("--exclude", nargs="*", help="exclude these files/paths from being restored")

    def handle(self, args):
        locations = []
        for policy in config.backup_policies:
            locations += policy.locations

        # remove duplicated locations
        locations = set(locations)

        for location in locations:
            action = RestoreBackupAction(
                id=f"CLI-{uuid.uuid4()}",
                location=location,
                snapshot_id=args.snapshot_id,
                target=args.target,
                include=args.path,
                exclude=args.exclude,
            )
            try:
                action.execute()
            except Exception as e:
                logging.debug(e, exc_info=True)
                logging.info(f"Not found at location {location.bucket_url}")
                continue
            else:
                logging.info(f"Backup restored from location {location.bucket_url}")
                break


class BackupNowCommand(Command):
    name = "now"
    help = "Execute Backup Policies Now"

    def add_arguments(self, parser):
        parser.add_argument("policies", nargs="*")

    def handle(self, args):
        if args.policies is None:
            policies = None
        else:
            policies = filter_objects(config.backup_policies, id__in=args.policies)

        action = BackupNowAction(id=f"CLI-{uuid.uuid4()}", policies=policies)
        action.execute()


class BackupCommand(Command):
    name = "backup"
    help = "Backup Actions"
    subcommands = (
        ListBackupSnapshotsCommand,
        BackupNowCommand,
        BackupRestoreCommand,
    )

    def handle(self, args):
        raise self.CommandError("You must specify a sub action.")
