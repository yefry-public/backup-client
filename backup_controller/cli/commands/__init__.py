from .config import *
from .service import *
from .backup import *

available_commands = [
    ConfigCommand,
    BackupCommand,
    RunDaemonCommand,
]
