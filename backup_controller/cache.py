"""
Cache utils module

Provides utilities to interact with filesystem cache
"""
import os
from pathlib import Path

from backup_controller.client.exceptions import ClientException
from backup_controller.conf import config
from backup_controller.utils import mkdir_p

bi_open = open

__all__ = (
    "CacheException",
    "exists",
    "create",
    "delete",
    "open",
)


class CacheException(ClientException):
    """
    Base exception for cache errors
    """


def exists(file: str):
    return os.path.isfile(get(file))


def list(path: str):
    path = Path(path)
    if path.is_absolute():
        raise CacheException("Cache file path cannot be absolute")

    return os.listdir(config.client_cache / path)


def create(file: str):
    path = Path(file)
    if path.is_absolute():
        raise CacheException("Cache file path cannot be absolute")

    # create path to file
    mkdir_p(config.client_cache / path.parent)

    # create file
    with bi_open(config.client_cache / file, 'w'):
        pass


def delete(file: str):
    if not exists(file):
        raise CacheException("File does not exist in cache")

    os.remove(config.client_cache / file)


def get(file: str):
    """
    Return absolute path to cached file

    :param file: Cached file
    :return: Absolute path to file
    """
    path = Path(file)
    if path.is_absolute():
        raise CacheException("Cache file path cannot be absolute")

    return config.client_cache / file


def open(file: str, mode="r"):
    try:
        return bi_open(get(file), mode)
    except OSError as e:
        raise CacheException(f"Could not open file: {e}") from e
