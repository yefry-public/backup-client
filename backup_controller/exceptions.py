
class ConfigurationError(Exception):
    """
    Base Exception for Configuration Errors.
    """


class ImproperlyConfigured(RuntimeError):
    """
    Exception for bad configuration in the code.

    This exception is raised whenever parts of the code fail runtime checks.
    """


class CommandError:
    """Exception for subprocess errors"""

    def __init__(self, exit_code, stdout=None, stderr=None, cmd=None):
        self.exit_code = exit_code
        self.cmd = cmd
        self.stdout = stdout
        self.stderr = stderr
        super(CommandError, self).__init__()

    def __str__(self):
        return f"CommandError: command '{self.cmd or 'restic'}' exited with status code {self.exit_code}\n" \
               f"STDOUT:\n{self.stdout}\n" \
               f"STDERR:\n{self.stderr}\n"
