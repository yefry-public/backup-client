"""
Local Client for local CLI control.
"""
import logging
import time
from datetime import datetime
from typing import Dict, Optional

from backup_controller import scheduler
from backup_controller.client.abstract import AbstractClient
from backup_controller.models import BackupPolicy


class LocalClient(AbstractClient):
    def __init__(self, base_url=None, **kwargs):
        self.backup_schedule: Dict[BackupPolicy, Optional[datetime]] = {}
        super(LocalClient, self).__init__(base_url, **kwargs)

    def run(self):
        while True:
            time.sleep(5)
            if not scheduler.is_running():
                logging.warning("Restarting")
                scheduler.start()
