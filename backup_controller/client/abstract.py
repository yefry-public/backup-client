import abc
import logging
import sys
from datetime import datetime
from typing import List, Optional

import pytz

from backup_controller import scheduler
from backup_controller.backends.abstract import BackupBackend, BackendException
from backup_controller.conf import config
from backup_controller.hooks.backup_hook import BackupHook
from backup_controller.hooks.exceptions import HookException
from backup_controller.models import BackupPolicy
from backup_controller.utils import cached_property, run_safe, get_object, terminate


class AbstractClient(abc.ABC):
    """
    Abstract Client.

    Implement this Interface to create a demonized client for remote control of the application.
    :param base_url: Base URL for the backend/remote end
    """

    def __init__(self, base_url, **kwargs):
        self.base_url = base_url
        self.setup()

    @cached_property
    def backend(self) -> BackupBackend:
        return config.backend_config.get_backend()

    @abc.abstractmethod
    def run(self):
        """
        Run the client forever. Listen for incoming messages and handle cron execution using threading.
        """

    def setup(self):
        self.update_schedule()
        scheduler.start()

    def stop(self):
        """
        Stop the client and terminate the execution.
        """
        logging.info("Stopping")
        try:
            scheduler.stop()
        except AttributeError:
            pass

        terminate()

    def on_error(self, exc: BackendException, **kwargs):
        """
        Optional backup error handler.
        """

    def on_success(self, **kwargs):
        """
        Optional backup success handler.
        """

    def on_stop(self):
        """
        Optional cleanup before exiting
        """

    def run_backup(self, policy: BackupPolicy, **kwargs):
        def run_hook(_hook_id):
            hook: BackupHook = get_object(config.hooks, id=_hook_id)
            if hook is None:
                logging.error(f"Hook {_hook_id} does not exist.")
                return

            logging.info(f"Executing hook '{hook.name}'")

            try:
                hook.execute()
            except HookException as e:
                logging.debug(e, exc_info=True)
                logging.error(f"Execution of hook '{hook.name}' failed, check logs for more details.")
            else:
                logging.info(f"Execution of hook '{hook.name}' completed")

        # check weekday is allowed
        if not datetime.now().weekday() in policy.allowed_days:
            logging.debug(f"Skipping backup for policy {policy.id}: day not allowed")
            return

        # run pre-hooks
        logging.debug(f"Running pre-hooks")
        for hook_id in policy.pre_hooks:
            run_hook(hook_id)

        logging.debug(f"Running backup")
        # execute backup
        try:
            self.backend.backup(policy.paths, policy.locations, policy.exclude_patterns)
        except BackendException as exc:
            self.on_error(exc=exc, **kwargs)
        else:
            self.on_success(**kwargs)
        finally:
            logging.debug(f"Running post-hooks")
            # run post-hooks
            for hook_id in policy.post_hooks:
                run_hook(hook_id)

            logging.info(f"Backup completed")

    def update_schedule(self, policies: Optional[List[BackupPolicy]] = None):
        """
        Update the Backup Schedule.

        :param policies: New Backup Policies
        """
        if policies is None:
            policies = config.backup_policies

        scheduler.schedule.clear()
        for policy in policies:
            if policy.schedule is None:
                logging.warning(f"Policy {policy.id} has no schedule.")
                continue

            for time_ in policy.schedule:
                scheduler.schedule.every().day.at(time_.isoformat(timespec="minutes")).tz(config.timezone).do(
                    run_safe(self.run_backup),
                    policy,
                )

        logging.debug(
            "Current Schedule:\n"
            + "\n".join(f"* {job.at_time} policy {job.job_func.args[0].id}" for job in scheduler.schedule.get_jobs())
        )
        next_run = scheduler.schedule.next_run()
        if next_run is None:
            logging.info(f"Schedule updated. No job scheduled.")
        else:
            delta = next_run - datetime.now(tz=pytz.timezone(config.timezone))
            logging.info(f"Schedule updated. Next job: {next_run} [{delta}]")
