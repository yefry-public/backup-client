"""
REST Client for Remote Control.
"""
from backup_controller.client.abstract import AbstractClient


class RestClient(AbstractClient):
    def __init__(self, base_url, **kwargs):
        super(RestClient, self).__init__(base_url, **kwargs)

    def run(self):
        raise NotImplementedError()
