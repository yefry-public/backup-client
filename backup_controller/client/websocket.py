"""
Websocket Client for Remote Control.
"""
import datetime
import logging
import os
import sys
import time
import uuid
from concurrent.futures import ThreadPoolExecutor
from threading import Lock
from typing import Union

from typing.io import IO
from websocket import WebSocket, ABNF, WebSocketException

from backup_controller.backends.abstract import BackendException
from backup_controller.client.abstract import AbstractClient
from backup_controller.conf import config
from backup_controller.conf.base import CA_CERTIFICATES
from backup_controller.models import Payload, Hook
from backup_controller.models.actions import GetSnapshotListAction
from backup_controller.models.messages import (
    ActionStatusMessage,
    ActionStatus,
    ClientConfigMessage,
    Message,
    BackupFailedMessage,
    BackupDoneMessage,
)


class WebsocketClient(AbstractClient):
    def __init__(self, base_url, access_token, **kwargs):
        self.access_token = access_token
        self.endpoint = base_url + ("" if base_url.endswith("/") else "/") + "ws/backup/client"
        self.client = WebSocket(sslopt={"ca_certs": CA_CERTIFICATES})
        self._send_lock = Lock()
        self._send_queue = []
        self._executor = ThreadPoolExecutor(max_workers=5, thread_name_prefix="WS_EXECUTOR")
        super(WebsocketClient, self).__init__(base_url, **kwargs)

    def _connect(self):
        while not self.client.connected:
            try:
                self.client.connect(
                    self.endpoint,
                    header={"Authorization": f"Bearer {self.access_token}"},
                )
            except (TimeoutError, ConnectionError, WebSocketException) as e:
                logging.warning(f"Connection failed: {e}")
                time.sleep(5)

        logging.info(f"Connected to {self.endpoint}")
        self.send(
            ClientConfigMessage(
                paths=config.allowed_paths, hooks=[Hook.parse_obj(hook.dict()) for hook in config.hooks]
            )
        )
        logging.info(f"Sent paths/hooks config")

    def send_file_chunked(self, file: IO, _chunk_size=4 * 1024 * 1024, **kwargs):
        """Send a chunked file"""
        with self._send_lock:
            meta = "&".join(f"{k}={v}" for k, v in kwargs.items())
            meta_start = ABNF.create_frame(f"S{meta};", ABNF.OPCODE_BINARY, 0)
            meta_continue = ABNF.create_frame(f"F{meta};", ABNF.OPCODE_BINARY, 0)
            meta_end = ABNF.create_frame(f"E{meta};", ABNF.OPCODE_BINARY, 1)

            # send start meta and first chunk of data
            self.client.send_frame(meta_start)
            data = file.read(_chunk_size)
            data_frame = ABNF.create_frame(data, ABNF.OPCODE_CONT, 1)
            self.client.send_frame(data_frame)

            # cycle other chunks
            data = file.read(_chunk_size)
            while data:
                self.client.send_frame(meta_continue)
                data_frame = ABNF.create_frame(data, ABNF.OPCODE_CONT, 1)
                self.client.send_frame(data_frame)
                data = file.read(_chunk_size)

            # send file upload end
            self.client.send_frame(meta_end)

    def send(self, data: Union[Message, str, bytes], text=True):
        """
        Send data through open websocket connection.

        :param data: Data to send
        :param text: Set to False for binary opcode
        """
        # todo queue
        logging.debug(f"Sending message: {data}")
        with self._send_lock:
            if text:
                opcode = ABNF.OPCODE_TEXT
            else:
                opcode = ABNF.OPCODE_BINARY

            if isinstance(data, Message):
                data = data.payload()

            return self.client.send(data, opcode=opcode)

    def on_success(self, action_id=None, **kwargs):
        self.send(
            BackupDoneMessage(backup_time=datetime.datetime.utcnow(), action_id=action_id),
        )
        # send updated snapshot list
        action = GetSnapshotListAction(id=f"AUTO-{uuid.uuid4()}")
        message = action.execute()
        self.send(message)

    def on_error(self, exc: BackendException, action_id=None, **kwargs):
        self.send(
            BackupFailedMessage(backup_time=datetime.datetime.utcnow(), reason=exc.get_reason(), action_id=action_id),
        )

    def _process_message(self, message):
        logging.debug(f"Message received from ws: {message}")
        try:
            payload = Payload.parse_raw(message)
            action = payload.object()
        except Exception as e:
            logging.error(e, exc_info=True)
            logging.critical(f"Could not process incoming message: {e}")
            return

        # todo: assert it's actually an action
        try:
            logging.info(f"Processing action: {action.id}.")
            self.send(ActionStatusMessage(action_id=action.id, status=ActionStatus.RUNNING).payload())
            response = action.execute()
        except Exception as e:
            logging.error(f"Action failed: {e}", exc_info=True)
            self.send(ActionStatusMessage(action_id=action.id, status=ActionStatus.FAILED).payload())
        else:
            if response is not None:
                self.send(response.payload())

            self.send(ActionStatusMessage(action_id=action.id, status=ActionStatus.SUCCESS).payload())

        logging.info(f"Action processed: {action.id}.")

    def _on_message(self, message):
        self._executor.submit(self._process_message, message)
        logging.debug(f"[ws] Message received and submitted")

    def run(self):
        self._connect()

        while True:
            try:
                self._on_message(self.client.recv())
            except WebSocketException:
                logging.info("Connection lost, reconnecting")
                self._connect()
            except Exception as e:
                logging.error(e, exc_info=True)

    def on_stop(self):
        self.client.close()
