"""
General uncategorized project utilities.
"""
import json
import logging
import sys
import os
from pathlib import Path
from typing import Union, List, TypeVar, Optional

from pydantic.json import pydantic_encoder

from . import schedule
from pydantic import BaseModel

try:
    from functools import cached_property
except ImportError:

    # Descriptor (non-data) for building an attribute on-demand on first use.
    #
    # This is a fallback for Python<3.8 and provides the missing :ref:``functools.cached_property``.
    # Snippet Source:
    # https://github.com/django/django/blob/adb4100e58d9ea073ee8caa454bb7c885b6a83ed/django/utils/functional.py
    # noinspection PyPep8Naming
    class cached_property:
        """
        Decorator that converts a method with a single self argument into a
        property cached on the instance.
        A cached property can be made out of an existing method:
        (e.g. ``url = cached_property(get_absolute_url)``).
        The optional ``name`` argument is obsolete as of Python 3.6 and will be
        deprecated in Django 4.0 (#30127).
        """

        name = None

        @staticmethod
        def func(instance):
            raise TypeError("Cannot use cached_property instance without calling __set_name__() on it.")

        def __init__(self, func, name=None):
            self.real_func = func
            self.__doc__ = getattr(func, "__doc__")

        def __set_name__(self, owner, name):
            if self.name is None:
                self.name = name
                self.func = self.real_func
            elif name != self.name:
                raise TypeError(
                    "Cannot assign the same cached_property to two different names (%r and %r)." % (self.name, name)
                )

        def __get__(self, instance, cls=None):
            """
            Call the function and put the return value in instance.__dict__ so that
            subsequent attribute access on the instance returns the cached value
            instead of calling cached_property.__get__().
            """
            if instance is None:
                return self
            res = instance.__dict__[self.name] = self.func(instance)
            return res


T = TypeVar("T", bound=BaseModel)


def mkdir_p(path: Union[str, Path]):
    """Emulate `mkdir -p path`"""
    path = str(path)

    if not os.path.isdir(path):
        part_dir = ""
        for subdir in path.split("/"):
            part_dir += subdir + "/"
            if not os.path.isdir(part_dir):
                os.mkdir(part_dir)


def get_all_subclasses(cls):
    """Recursively get all subclasses of ``cls``"""
    all_subclasses = []

    for subclass in cls.__subclasses__():
        all_subclasses.append(subclass)
        all_subclasses.extend(get_all_subclasses(subclass))

    return all_subclasses


def filter_objects(objects: List[T], **query) -> List[T]:
    """
    Filter objects of a list of objects.

    Filters can include the following modifiers:
      - startswith
      - endswith
      - contains
      - icontains
      - in
    Modifiers can be used django-style: field__startswith="example"

    :param objects: List of objects to filter
    :param query: Attribute-Value filter
    :return: Filtered list of objects
    """

    def filter_func(key, obj, value):
        """
        Return True if ``obj`` matches a query.

        :param key: Query Key (with modifiers)
        :param obj: Object to check
        :param value: Value to match
        :return: True if object matches
        """
        if "__" in key:
            if key.count("__") > 1:
                raise ValueError("Query cannot contain more than one modifier")
            query_key, query_modifier = key.split("__")
        else:
            query_key = key
            query_modifier = None

        if query_modifier is None:
            operator = lambda x, y: x == y
        elif query_modifier == "in":
            operator = lambda x, y: x in y
        elif query_modifier == "startswith":
            operator = lambda x, y: str.startswith(x, y)
        elif query_modifier == "endswith":
            operator = lambda x, y: str.endswith(x, y)
        elif query_modifier == "contains":
            operator = lambda x, y: y in x
        elif query_modifier == "icontains":
            operator = lambda x, y: str.lower(y) in str.lower(x)
        else:
            raise ValueError(f"Invalid modifier: {query_modifier}")

        try:
            return operator(getattr(obj, query_key), value)
        except AttributeError:
            return False

    results = filter(
        lambda obj: all(filter_func(key, obj, value) for key, value in query.items()),
        objects,
    )
    return list(results)


def get_object(objects: List[T], __safe=False, **query) -> Optional[T]:
    """
    Get an object from a list of objects.

    :param objects: List of objects to search
    :param __safe: Return first object instead of raising ValueError if multiple objects are found
    :param query: Attribute-Value filter
    :raise ValueError: If multiple objects returned
    :return: Found object or None
    """
    filtered_objects = filter_objects(objects, **query)
    if len(filtered_objects) == 0:
        return None
    if len(filtered_objects) == 1 or __safe:
        return filtered_objects[0]

    raise ValueError("More than one object returned.")


def run_safe(func):
    """
    Decorator to run a job safely in scheduler.
    """

    def inner(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logging.debug(e, exc_info=True)
            logging.critical(e)

    return inner


def run_once_safe(func):
    """
    Decorator to run a job only once in scheduler (safely).
    """

    def inner(*args, **kwargs):
        try:
            func(*args, **kwargs)
        except Exception as e:
            logging.debug(e, exc_info=True)
            logging.critical(e)
        finally:
            return schedule.CancelJob

    return inner


def is_subpath(path: Union[Path, str], parent: Union[Path, str]):
    """
    Return true if ``path`` is a subpath of ``parent``.

    :param path: Path that should be a subpath
    :param parent: Parent Path
    :return: True if ``path`` is a subpath of ``parent``
    """
    if not Path(path).is_absolute() or not Path(parent).is_absolute():
        raise ValueError("Absolute paths are required.")

    sub_path = str(path)
    parent_path = str(parent)
    if not sub_path.endswith("/"):
        sub_path += "/"
    if not parent_path.endswith("/"):
        parent_path += "/"

    return sub_path.startswith(parent_path)


empty = object()


def cached_method(func):
    """
    Cache method result and return it on every later call.

    Useful to implement singletons. Similar to cached_property, but returns a callable.
    """
    result = empty

    def inner(*args, **kwargs):
        nonlocal result
        if result is empty:
            result = func(*args, **kwargs)
        return result

    return inner


def as_json(data: Union[dict, list]) -> str:
    return json.dumps(data, default=pydantic_encoder)


# noinspection PyShadowingBuiltins
def terminate(code: int = 0):
    """
    Exit the program

    This function wraps os._exit to make sure the program exits even if compiled

    :param code: terminate code
    """

    # noinspection PyUnresolvedReferences,PyProtectedMember
    os._exit(code)


def get_base_dir():
    # Check if running from PyInstaller executable (one-directory mode)
    if hasattr(sys, '_MEIPASS'):
        return Path(sys._MEIPASS)
    else:
        # Running as a script or from a development environment
        return Path(__file__).resolve().parent