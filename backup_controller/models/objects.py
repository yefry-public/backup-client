"""
Models for API Objects, such as backup policies and locations.
"""
import datetime
import uuid
from pathlib import Path
from typing import Optional, List

from pydantic import BaseModel, PositiveInt, Field, NonNegativeInt, SecretStr

__all__ = (
    "BackupPolicy",
    "RetentionPolicy",
    "Location",
    "BackupSnapshot",
    "SnapshotContent",
    "SnapshotNode",
    "Hook",
)


class RetentionPolicy(BaseModel):
    keep_last: Optional[PositiveInt]
    keep_hourly: Optional[PositiveInt]
    keep_daily: Optional[PositiveInt]
    keep_weekly: Optional[PositiveInt]
    keep_monthly: Optional[PositiveInt]
    keep_yearly: Optional[PositiveInt]


class Location(BaseModel):
    bucket_url: str
    access_key_id: str
    secret_access_key: SecretStr
    retention_policy: RetentionPolicy = RetentionPolicy()

    def __hash__(self):
        return hash(self.bucket_url)


class BackupPolicy(BaseModel):
    id: str = Field(default_factory=uuid.uuid4)
    paths: List[Path]
    schedule: Optional[List[datetime.time]]
    exclude_patterns: List[str] = Field(default_factory=list)
    allowed_days: List[int] = Field(default=list(range(7)))  # default every weekday
    locations: List[Location] = Field(default_factory=list)
    pre_hooks: List[str] = Field(default_factory=list)
    post_hooks: List[str] = Field(default_factory=list)

    def __hash__(self):
        return hash(self.id)


class BackupSnapshot(BaseModel):
    id: str
    parent: Optional[str]
    paths: List[Path]
    time: datetime.datetime


class SnapshotNode(BaseModel):
    name: str
    is_dir: bool = True
    mode: Optional[str]
    size: Optional[NonNegativeInt]
    gid: Optional[str]
    uid: Optional[str]
    atime: Optional[str]
    mtime: Optional[str]
    ctime: Optional[str]


class SnapshotContent(BaseModel):
    snapshot_id: str
    content: SnapshotNode


class Hook(BaseModel):
    id: str = Field(default_factory=uuid.uuid4)
    name: str
    description: str = ""
