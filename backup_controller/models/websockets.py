"""
Websocket Payload Serialization utilities.
"""
import abc
from typing import Dict, Any, Type

from pydantic import BaseModel

from backup_controller.utils import get_all_subclasses

__all__ = ("Payload", "WebsocketSerializableMixin")


class WebsocketSerializableMixin(abc.ABC):
    __obj_type__: str

    def payload_object(self):
        """
        Return Payload for this object

        :return: Payload for this object
        """
        return Payload(obj_type=self.__obj_type__, obj=self)

    def payload(self):
        """
        Return object as Payload serialized for transmission.

        :return: Serialized Payload
        """
        return self.payload_object().json().encode("utf-8")


class Payload(BaseModel):
    __obj_map__ = None
    obj_type: str
    obj: Dict[str, Any]

    def __new__(cls, *args, **kwargs):
        if cls.__obj_map__ is None:
            cls.__obj_map__ = get_objects_map()
        return super().__new__(cls)

    def object(self):
        try:
            return self.__obj_map__[self.obj_type].parse_obj(self.obj)
        except KeyError:
            raise RuntimeError("Invalid Payload")


def get_objects_map() -> Dict[str, Type[BaseModel]]:
    models = list(
        filter(
            lambda c: hasattr(c, "__obj_type__"),
            get_all_subclasses(WebsocketSerializableMixin),
        )
    )
    return {c.__obj_type__: c for c in models}
