"""
Models for messages sent client to remote control dashboards.
"""
import abc
import datetime
import uuid
from enum import Enum
from pathlib import Path
from typing import List, Optional

from pydantic import BaseModel, Field

from .objects import BackupSnapshot, Hook, SnapshotNode
from .websockets import WebsocketSerializableMixin

__all__ = (
    "ActionStatus",
    "MessageType",
    "Message",
    "ActionStatusMessage",
    "BackupFailedMessage",
    "BackupDoneMessage",
    "ClientConfigMessage",
    "SnapshotListMessage",
    "SnapshotContentMessage",
)


class ActionStatus(str, Enum):
    SCHEDULED = "scheduled"
    RUNNING = "running"
    SUCCESS = "success"
    FAILED = "failed"


class MessageType(str, Enum):
    ACTION_STATUS = "msg.action_status"
    BACKUP_FAILED = "msg.backup_failed"
    BACKUP_DONE = "msg.backup_done"
    SNAPSHOT_LIST = "msg.snapshot.list"
    SNAPSHOT_CONTENT = "msg.snapshot.content"
    CLIENT_CONFIG = "msg.client_config"


class Message(BaseModel, WebsocketSerializableMixin, abc.ABC):
    """
    Base Message Model
    """

    id: str = Field(default_factory=uuid.uuid4)


class ActionStatusMessage(Message):
    __obj_type__ = MessageType.ACTION_STATUS

    action_id: str
    status: ActionStatus


class BackupFailedMessage(Message):
    __obj_type__ = MessageType.BACKUP_FAILED

    backup_time: datetime.datetime
    reason: str
    action_id: Optional[str] = None


class BackupDoneMessage(Message):
    __obj_type__ = MessageType.BACKUP_DONE

    backup_time: datetime.datetime
    action_id: Optional[str] = None


class ClientConfigMessage(Message):
    """
    Message to send client config options like allowed paths and hooks.
    """

    __obj_type__ = MessageType.CLIENT_CONFIG

    paths: List[Path] = Field(default_factory=list)
    hooks: List[Hook] = Field(default_factory=list)


class SnapshotListMessage(Message):
    __obj_type__ = MessageType.SNAPSHOT_LIST

    snapshots: List[BackupSnapshot]


class SnapshotContentMessage(Message):
    __obj_type__ = MessageType.SNAPSHOT_CONTENT

    snapshot_id: str
    path: str
    content: List[SnapshotNode]
