"""
Action Models.

These models represent requests from remote control backends that are meant to trigger an action on this client.
"""

import abc
import logging
from enum import Enum
from pathlib import Path
from typing import List, Optional

import pytz
from pydantic import BaseModel, validator

from backup_controller.conf import config
from backup_controller.utils import get_object
from .messages import Message, SnapshotContentMessage, SnapshotListMessage
from .objects import Location, BackupPolicy
from .websockets import WebsocketSerializableMixin
from ..backends.abstract import BackendException

__all__ = (
    "ActionType",
    "Action",
    "BackupNowAction",
    "RestoreBackupAction",
    "GetSnapshotContentAction",
    'GetSnapshotListAction',
    "UpdateClientConfigAction",
)


logger = logging.getLogger(__name__)


class ValidatePathMixin:
    @staticmethod
    def validate_existing_path(value):
        path = get_object(config.backup_paths, path=value)
        if path is None:
            raise ValueError("Invalid Path")
        return value


class ActionType(str, Enum):
    BACKUP_NOW = "action.backup_now"
    BACKUP_RESTORE = "action.restore"
    UPDATE_CONFIG = "action.update_config"
    GET_SNAPSHOT_LIST = "action.snapshots.list"
    GET_SNAPSHOT_CONTENT = "action.snapshots.get_content"
    GET_SNAPSHOT_FILE = "action.snapshots.get_file"


class Action(BaseModel, WebsocketSerializableMixin, abc.ABC):
    """
    Abstract Action class

    Represents a command given from the dashboard to a client.
    """

    id: str

    @abc.abstractmethod
    def execute(self) -> Optional[Message]:
        """
        Override this method with the business logic needed to execute this action.
        Return a response object or None.
        """


class BackupNowAction(Action):
    __obj_type__ = ActionType.BACKUP_NOW

    policies: Optional[List[BackupPolicy]] = None

    def execute(self):
        # backup all policies if policies attribute is None or empty
        policies = self.policies or config.backup_policies

        for policy in policies:
            config.client.run_backup(policy, action_id=self.id)


class RestoreBackupAction(Action):
    __obj_type__ = ActionType.BACKUP_RESTORE

    location: Location
    snapshot_id: str
    target: Optional[str] = None
    include: Optional[List[str]] = None
    exclude: Optional[List[str]] = None

    def execute(self):
        config.backend.snapshot(self.location, self.snapshot_id).restore(
            target=self.target, include=self.include, exclude=self.exclude
        )


class GetSnapshotContentAction(Action):
    __obj_type__ = ActionType.GET_SNAPSHOT_CONTENT

    snapshot_id: str
    path: str

    def execute(self) -> SnapshotContentMessage:
        locations = []
        for policy in config.backup_policies:
            locations += policy.locations

        data = None
        for location in locations:
            data = config.backend.snapshot(location, self.snapshot_id).ls(path=self.path)
            # empty list if snapshot not found
            if data:
                break

        return SnapshotContentMessage(snapshot_id=self.snapshot_id, path=self.path, content=data)


class GetSnapshotListAction(Action):
    __obj_type__ = ActionType.GET_SNAPSHOT_LIST

    def execute(self) -> SnapshotListMessage:
        locations = []
        for policy in config.backup_policies:
            locations += policy.locations

        snapshots = []
        locations = set(locations)
        for location in locations:
            try:
                snapshots += config.backend.list(location)
            except BackendException as e:
                logger.error(e)

        return SnapshotListMessage(snapshots=snapshots)


class GetSnapshotFileContentAction(Action):
    __obj_type__ = ActionType.GET_SNAPSHOT_FILE

    snapshot_id: str
    file: str

    def execute(self):
        from backup_controller.backends.abstract import BackendException

        file_path = Path(self.file)
        locations = []
        for policy in config.backup_policies:
            locations += policy.locations

        file_list = None
        location = None
        for location in locations:
            file_list = config.backend.snapshot(location, self.snapshot_id).ls(path=str(file_path.parent))
            # empty list if snapshot not found
            if file_list:
                break

        if file_list is None:
            raise FileNotFoundError("Snapshot not found")

        node = next(filter(lambda n: n.name == self.file.split("/")[-1], file_list), None)
        if node is None:
            raise FileNotFoundError("File not found")

        filename = node.name
        if node.is_dir:
            filename += ".zip"

        try:
            stream = config.backend.snapshot(location, self.snapshot_id).get(self.file, zip_=node.is_dir)
        except BackendException as e:
            raise FileNotFoundError(f"Could not retrieve file: {e.get_reason()}")

        if not hasattr(config.client, "send_file_chunked"):
            raise RuntimeError("Client does not support chunked file send")

        config.client.send_file_chunked(stream, filename=filename, snapshot_id=self.snapshot_id, file_path=self.file)


class UpdateClientConfigAction(Action):
    __obj_type__ = ActionType.UPDATE_CONFIG

    backup_policies: List[BackupPolicy]
    master_password: str
    timezone: str

    def execute(self):
        config.backup_policies = self.backup_policies
        config.master_password = self.master_password
        config.timezone = self.timezone
        config.save()
        config.client.update_schedule()

    # validators are class methods
    # noinspection PyMethodParameters
    @validator("timezone")
    def validate_tz(cls, value):
        if value not in pytz.all_timezones:
            raise ValueError("Invalid timezone")
        return value
