import json
import logging
import secrets
import sys
from typing import List, Union

import yaml
from pydantic import ValidationError, SecretStr

from backup_controller.models import BackupPolicy
from backup_controller.utils import mkdir_p, is_subpath, terminate, get_base_dir
from backup_controller.hooks.backup_hook import BackupHook
from backup_controller.hooks.default_hooks import default_hooks

from .base import *
from .models import *


class Config(BaseModel):
    restic_path: Path = RESTIC_PATH
    client_cache: Path = CONFIG_DIR / "cache/client"
    restic_cache: Path = CONFIG_DIR / "cache/restic"

    timezone: str = "UTC"
    allowed_paths: List[Path] = ['/']
    master_password: str = secrets.token_urlsafe(32)
    backup_policies: List[BackupPolicy] = []
    hooks: List[BackupHook] = default_hooks

    client_config: Union[
        RestClientConfig,
        WebsocketClientConfig,
        LocalClientConfig,
    ] = LocalClientConfig()

    backend_config: Union[
        DummyBackendConfig,
        ResticBackendConfig,
    ] = DummyBackendConfig()

    class Config:
        json_encoders = {
            SecretStr: lambda v: v.get_secret_value() if v else None,
        }

    @classmethod
    def create_hooks_folder(cls):
        hooks_folder = CONFIG_DIR / "hooks"

        if not hooks_folder.exists():
            hooks_folder.mkdir(parents=True)

        #Create default hooks
        base_dir = get_base_dir()
        hooks_directory =  base_dir / "backup_controller/hooks/default_hooks"

        for hook_file in hooks_directory.iterdir():
            if hook_file.is_file():
                hook_name = hook_file.name
                hook_destination = hooks_folder / hook_name
                with hook_file.open('rb') as src_file, hook_destination.open('wb') as dst_file:
                    dst_file.write(src_file.read())
                print(f"Hook '{hook_name}' copied to the hooks folder.")

    @classmethod
    def __create_new_config(cls, config_file: Path):
        mkdir_p(config_file.parent)
        conf = cls()
        conf.save()
        return conf
    
    

    @classmethod
    def load(cls):
        if not os.path.isfile(CONFIG_FILE):
            config = cls.__create_new_config(CONFIG_FILE)
            config.create_hooks_folder() 
            return config

        with open(CONFIG_FILE, "r") as config:
            config_data = yaml.safe_load(config)

        try:
            obj = cls.parse_obj(config_data)
        except ValidationError as e:
            logging.error(f"Invalid configuration at {CONFIG_FILE}: {e}")
            logging.info(f"Fix the error manually or delete the configuration to generate a fresh one.")
            terminate(1)

        try:
            obj.sanity_check()
        except ValueError as e:
            logging.error(f"Invalid configuration at {CONFIG_FILE}: {e}")
            logging.info(f"Fix the error manually or delete the configuration to generate a fresh one.")
            terminate(1)

        return obj

    def save(self, config_path: Path = CONFIG_FILE):
        try:
            self.sanity_check()
        except ValueError:
            logging.error(f"Invalid configuration, can not save to {CONFIG_FILE}")
            raise

        with open(config_path, "w") as f:
            # serialize to json, then reload such that only basic types are used and secrets are dumped correctly
            yaml.dump(json.loads(self.json()), f)

    @property
    def backend(self):
        return self.backend_config.get_backend()

    @property
    def client(self):
        return self.client_config.get_client()

    def sanity_check(self):
        for policy in self.backup_policies:
            # check paths are allowed
            for path in policy.paths:
                if not Path(path).is_absolute():
                    raise ValueError(f"Backup Policy requests a relative path: {path}")

                if not any(is_subpath(path, allowed) for allowed in self.allowed_paths):
                    raise ValueError(f"Backup Policy requests a path that is not allowed: {path}")

            # check pre hooks exist
            for hook_id in policy.pre_hooks:
                if not any(hook.id == hook_id for hook in self.hooks):
                    raise ValueError(f"Backup Policy requests a Hook that does not exist: {hook_id}")

            # check post hooks exist
            for hook_id in policy.post_hooks:
                if not any(hook.id == hook_id for hook in self.hooks):
                    raise ValueError(f"Backup Policy requests a Hook that does not exist: {hook_id}")
