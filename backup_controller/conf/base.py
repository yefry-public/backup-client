"""
This module defines project configuration and load/store handlers for it.
"""
import os
from pathlib import Path
import certifi


# base paths

BASE_DIR = Path(__file__).parent.parent
USER = os.environ.get("SUDO_USER", os.environ.get("USER", "root"))
HOME = Path(os.path.expanduser("~" + USER))
USER_CONFIG_DIR = HOME / ".config/innovativity/backup-client"

CONFIG_DIR = Path(os.environ.get("BACKUP_CTRL_CONFIG_DIR", USER_CONFIG_DIR))
CONFIG_FILE = CONFIG_DIR / "config.yml"

CA_CERTIFICATES = os.environ.get("BACKUP_CTRL_CA_CERTIFICATES", certifi.where())
RESTIC_PATH = os.environ.get("BACKUP_CTRL_RESTIC_PATH", "/usr/bin/restic")

LOG_FILE = CONFIG_DIR / "logs/log.txt"
LOG_LEVEL = os.environ.get("BACKUP_CTRL_LOG_LEVEL", "INFO")
