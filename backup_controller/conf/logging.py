import logging
from logging.handlers import RotatingFileHandler
from typing import Optional

from .base import *
from backup_controller.utils import mkdir_p


class NoTracebackStreamHandler(logging.StreamHandler):
    """
    Custom StreamHandler that disables tracebacks and replaces them with a note to check other logs.

    Based on https://stackoverflow.com/a/54605728
    """

    def handle(self, record):
        exc_info, exc_text = record.exc_info, record.exc_text
        if record.exc_info:
            record.exc_info = None
            record.exc_text = "Check the logs for more information about this error"

        try:
            super().handle(record)
        finally:
            record.exc_info = exc_info
            record.exc_text = exc_text


def setup_logging(
    log_file: Optional[Path] = LOG_FILE,
    log_max_bytes: int = 10 * 1024 * 1024,
    log_file_count: int = 3,
    level: int = LOG_LEVEL,
):
    """
    Setup Logging

    :param log_file: Path to the log file (rotating). Set to None to disable logging to file.
    :param log_max_bytes: Max bytes before rotating log file
    :param log_file_count: Max number of rotated log files
    :param level: Log level
    """

    logging.root.setLevel(level)

    if os.environ.get("BACKUP_CTRL_LOGGING_FORCE_TRACEBACKS"):
        streamHandler = logging.StreamHandler()
    else:
        streamHandler = NoTracebackStreamHandler()

    streamHandler.setFormatter(
        logging.Formatter("%(asctime)-15s [%(processName)s/%(threadName)s] %(levelname)s: %(message)s")
    )
    streamHandler.setLevel(level)
    logging.root.addHandler(streamHandler)

    if log_file is None:
        logging.debug("Logging to file disabled")
        return

    if not os.path.isdir(log_file.parent):
        mkdir_p(log_file.parent)

    fileHandler = RotatingFileHandler(log_file, mode="a", maxBytes=log_max_bytes, backupCount=log_file_count)
    fileHandler.setFormatter(logging.Formatter("%(asctime)-15s %(levelname)s: %(message)s"))
    fileHandler.setLevel(level)
    logging.root.addHandler(fileHandler)
