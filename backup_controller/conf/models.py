import abc
from typing import Type, Optional

from pydantic import BaseModel, Field

from backup_controller.backends.abstract import BackupBackend
from backup_controller.backends.dummy import DummyBackend
from backup_controller.backends.restic import ResticBackend
from backup_controller.client.abstract import AbstractClient
from backup_controller.client.local import LocalClient
from backup_controller.client.rest import RestClient
from backup_controller.client.websocket import WebsocketClient
from backup_controller.utils import cached_method


# --- Clients ----------------------------------------------------------------------------------------------------------


class ClientConfig(BaseModel, abc.ABC):
    """Base model for Client Configuration"""

    __client_class__: Type[AbstractClient]

    base_url: str

    @cached_method
    def get_client(self) -> AbstractClient:
        return self.__client_class__(**self.dict())


class RestClientConfig(ClientConfig):
    __client_class__ = RestClient
    client_class = Field(const=True, default=__client_class__.__name__)


class WebsocketClientConfig(ClientConfig):
    __client_class__ = WebsocketClient
    client_class = Field(const=True, default=__client_class__.__name__)
    access_token: str


class LocalClientConfig(ClientConfig):
    __client_class__ = LocalClient
    client_class = Field(const=True, default=__client_class__.__name__)

    base_url: str = ""


# --- Backup Backends --------------------------------------------------------------------------------------------------

class BackupBackendConfig(BaseModel, abc.ABC):
    """Base model for Backup Backend Configuration"""

    __backend_class__: Type[BackupBackend]

    @cached_method
    def get_backend(self) -> BackupBackend:
        return self.__backend_class__()


class ResticBackendLimitsConfig(BaseModel):
    cpu: Optional[str] = Field(default=None, regex=r"^\d+(\.\d+)?s$")
    mem: Optional[str] = Field(default=None, regex=r"^\d+[MG]$")

    @cached_method
    def get_cpu_seconds(self) -> Optional[float]:
        if self.cpu is None:
            return None

        # remove the trailing "s" and convert to float
        return float(self.cpu[:-1])

    @cached_method
    def get_mem_bytes(self) -> Optional[int]:
        if self.mem is None:
            return None

        # remove the trailing "M/G" and convert to bytes
        unit = self.mem[-1]
        value = int(self.mem[:-1])

        # to megabytes
        value *= 1024 * 1024

        if unit == "G":
            # to gigabytes
            value *= 1024

        return value


class ResticBackendConfig(BackupBackendConfig):
    __backend_class__ = ResticBackend
    backend_class = Field(const=True, default=__backend_class__.__name__)

    enable_ls_dir_sizes: bool = True
    gocd_value: int = 20  # aggressive garbage collection by default
    limits: ResticBackendLimitsConfig = Field(default_factory=ResticBackendLimitsConfig)


class DummyBackendConfig(BackupBackendConfig):
    __backend_class__ = DummyBackend
    backend_class = Field(const=True, default=__backend_class__.__name__)
