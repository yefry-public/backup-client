import operator

from .logging import setup_logging
from .signals import setup_signals

__all__ = ("config", "setup_logging", "setup_signals")


empty = object()


# from django codebase
# https://github.com/django/django/blob/3f0025c18a08535ed39a64c24174f7e2d75b7b9e/django/utils/functional.py#L245
def new_method_proxy(func):
    def inner(self, *args):
        if self._config is empty:
            self._setup()
        return func(self._config, *args)
    return inner


class LazyConfig:
    """
    Lazy Config Wrapper.

    Wraps configuration for lazy loading to avoid circular imports around the project.
    Heavily based on django's implementation:
    https://github.com/django/django/blob/3f0025c18a08535ed39a64c24174f7e2d75b7b9e/django/utils/functional.py#L253
    """
    _config = None

    def __init__(self):
        self._config = empty

    def _setup(self):
        from .conf import Config
        self._config = Config.load()

    __getattr__ = new_method_proxy(getattr)

    def __setattr__(self, name, value):
        if name == "_config":
            # Assign to __dict__ to avoid infinite __setattr__ loops.
            self.__dict__["_config"] = value
        else:
            if self._config is empty:
                self._setup()
            setattr(self._config, name, value)

    def __delattr__(self, name):
        if name == "_config":
            raise TypeError("can't delete _config.")
        if self._config is empty:
            self._setup()
        delattr(self._config, name)

    __bytes__ = new_method_proxy(bytes)
    __str__ = new_method_proxy(str)
    __bool__ = new_method_proxy(bool)

    # Introspection support
    __dir__ = new_method_proxy(dir)

    # Need to pretend to be the wrapped class, for the sake of objects that
    # care about this (especially in equality tests)
    __class__ = property(new_method_proxy(operator.attrgetter("__class__")))
    __eq__ = new_method_proxy(operator.eq)
    __lt__ = new_method_proxy(operator.lt)
    __gt__ = new_method_proxy(operator.gt)
    __ne__ = new_method_proxy(operator.ne)
    __hash__ = new_method_proxy(hash)

    # List/Tuple/Dictionary methods support
    __getitem__ = new_method_proxy(operator.getitem)
    __setitem__ = new_method_proxy(operator.setitem)
    __delitem__ = new_method_proxy(operator.delitem)
    __iter__ = new_method_proxy(iter)
    __len__ = new_method_proxy(len)
    __contains__ = new_method_proxy(operator.contains)


config = LazyConfig()
