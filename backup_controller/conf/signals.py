import signal


def sigterm_handler(_signo, _stack_frame):
    from backup_controller.conf import config

    config.client_config.get_client().stop()


def setup_signals():
    signal.signal(signal.SIGTERM, sigterm_handler)
