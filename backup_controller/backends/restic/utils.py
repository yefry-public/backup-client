import logging
import resource
import subprocess
from typing import List, Union, Dict, Any

from backup_controller.conf import config
from backup_controller.models import Location
from .exceptions import ResticCommandError

logger = logging.getLogger(__name__)


def __validate_input(value: str):
    if not isinstance(value, str):
        raise TypeError("Value is not a string")


def __preexec_redis_subprocess():
    # apply resource limits after forking redis subprocess
    # be aware this function is not thread safe, do not change stuff here
    cpu_seconds_limit = config.backend_config.limits.get_cpu_seconds()
    mem_bytes_limit = config.backend_config.limits.get_mem_bytes()

    if cpu_seconds_limit:
        resource.setrlimit(resource.RLIMIT_CPU, (cpu_seconds_limit, cpu_seconds_limit))
    if mem_bytes_limit:
        resource.setrlimit(resource.RLIMIT_AS, (mem_bytes_limit, mem_bytes_limit))


def __get_command(command, location, *args, env=None, **kwargs) -> (tuple, Dict[str, Any]):
    """
    Return command and options for subprocess

    :param command: Restic command ("backup", "init", "forget", ...)
    :param location: Location with restic repository (S3 compatible)
    :param env: optional extra environments settings
    :param args: positional arguments for the command
    :param kwargs: named arguments for the command. Keys will replace _ with - and append a -- suffix. If value is a
    :return: command as tuple, Popen options
    """
    __validate_input(command)
    __validate_input(location.bucket_url)
    for val in args:
        __validate_input(val)
    for key, value in kwargs.items():
        __validate_input(key)
        if isinstance(value, list):
            for val in value:
                __validate_input(str(val or ""))
        else:
            __validate_input(str(value or ""))

    repository = f"s3:{location.bucket_url}"
    env = env or {}
    env.update(
        {
            "AWS_ACCESS_KEY_ID": location.access_key_id,
            "AWS_SECRET_ACCESS_KEY": location.secret_access_key.get_secret_value(),
            "RESTIC_PASSWORD": config.master_password,
            "GOCD": str(config.backend_config.gocd_value),
            "RESTIC_CACHE_DIR": config.restic_cache,
        }
    )

    named_arguments: List[str] = []
    for key, value in kwargs.items():
        arg = key.replace("_", "-")
        if isinstance(value, list):
            for val in value:
                named_arguments.append(f"--{arg}")
                named_arguments.append(str(val))
        else:
            named_arguments.append(f"--{arg}")
            if value is not None:
                named_arguments.append(str(value))

    opts = dict(
        shell=False,
        env=env,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        preexec_fn=__preexec_redis_subprocess,
    )

    return (
        str(config.restic_path),
        "-r",
        repository,
        command,
        *args,
        *named_arguments,
    ), opts


def stream_restic_shell(command: str, location: Location, *args, env=None, **kwargs) -> subprocess.Popen:
    """
    Run and stream stdout and stderr of a Restic subprocess.

    :param command: Restic command ("backup", "init", "forget", ...)
    :param location: Location with restic repository (S3 compatible)
    :param env: Environment variables
    :param args: positional arguments for the command
    :param kwargs: named arguments for the command. Keys will replace _ with - and append a -- suffix. If value is a
    list, then the argument will be repeated for each element. If the list is empty, the argument will be omitted
    entirely.
    :return: process object
    """
    cmd, opts = __get_command(command, location, *args, env=env, **kwargs)
    return subprocess.Popen(
        cmd,
        **opts
    )


def run_restic_shell(command: str, location: Location, *args, env=None, binary=False, **kwargs) -> Union[str, bytes]:
    """
    Run a Restic subprocess.

    :param command: Restic command ("backup", "init", "forget", ...)
    :param location: Location with restic repository (S3 compatible)
    :param env: Environment variables
    :param binary: Return binary output
    :param args: positional arguments for the command
    :param kwargs: named arguments for the command. Keys will replace _ with - and append a -- suffix. If value is a
    list, then the argument will be repeated for each element. If the list is empty, the argument will be omitted
    entirely.
    :return: output (stdout)
    """
    cmd, opts = __get_command(command, location, *args, env=env, **kwargs)
    process = subprocess.run(
        cmd,
        **opts,
    )
    output = process.stdout
    stderr = process.stderr.decode("utf-8")

    if not binary:
        output = output.decode("utf-8")

    exit_code = process.returncode
    # too much output
    # logger.debug(f"Command Output: [Exit {exit_code}] \nSTDOUT: {output}\nSTDERR: {stderr}")
    logger.debug(f"Command result: [Exit {exit_code}] STDERR: {stderr}")
    if exit_code != 0:
        raise ResticCommandError(exit_code, stdout=output, stderr=stderr, cmd=cmd)
    return output
