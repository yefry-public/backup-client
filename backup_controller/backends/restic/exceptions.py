from backup_controller.exceptions import CommandError
from backup_controller.backends.abstract import BackendException

__all__ = (
    "ResticCommandError",
    "SnapshotDoesNotExist",
)


class ResticCommandError(BackendException, CommandError):
    """Exception for subprocess errors"""

    def __init__(self, exit_code, stdout=None, stderr=None, cmd=None):
        self.exit_code = exit_code
        self.cmd = cmd
        self.stdout = stdout
        self.stderr = stderr
        super(ResticCommandError, self).__init__()

    def __str__(self):
        return (
            f"CommandError: command '{self.cmd or 'restic'}' exited with status code {self.exit_code}\n"
            f"STDOUT:\n{self.stdout}\n"
            f"STDERR:\n{self.stderr}\n"
        )

    def get_reason(self):
        return self.stderr or self.stdout


class SnapshotDoesNotExist(BackendException):
    """
    Exception for invalid snapshot requests
    """


class BackupFailed(BackendException):
    def get_reason(self):
        return str(self)
