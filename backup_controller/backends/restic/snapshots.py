import json
from hashlib import sha256
from typing import Optional, List, IO

from backup_controller.conf import config
from backup_controller.backends.abstract import SnapshotHandler
from backup_controller.models.objects import SnapshotNode
from backup_controller.utils import cached_property
from .utils import run_restic_shell, stream_restic_shell

__all__ = ("ResticSnapshotHandler",)


class ResticSnapshotHandler(SnapshotHandler):
    def ls(self, path: str) -> List[SnapshotNode]:
        recursive = getattr(config.backend_config, "enable_ls_dir_sizes", True)

        # add slash at the end of path
        # we only expect directories as input paths
        if not path.endswith("/"):
            path += "/"

        if recursive:
            output = stream_restic_shell("ls", self.location, self.snapshot_id, path, recursive=None, json=None)
        else:
            output = stream_restic_shell("ls", self.location, self.snapshot_id, path, json=None)

        nodes: List[SnapshotNode] = []

        for line in output.stdout.readlines():
            if not line:
                continue

            data = json.loads(line)
            data_path = data.get("path")

            # ignore empty paths
            if not data_path:
                continue

            # ignore paths that are unrelated with the requested path
            # should not happen (path filtered in restic command)
            if not data_path.startswith(path):
                continue

            # ignore the same path
            if data_path == path:
                continue

            # strip the requested path and get subpath
            # for example:
            # path = "/test/something/"
            # data_path = "/another" => ignored
            # data_path = "/test/something/file1" => sub_path = "file1"
            # data_path = "/test/something/subdir/file" => sub_path = "subdir/file1"
            sub_path = data_path[len(path):]
            sub_path_parts = sub_path.split("/")

            # if the base path is not in nodes, add it
            # otherwise, get it
            base_path = next(filter(lambda n: n.name == sub_path_parts[0], nodes), None)
            if base_path is None:
                base_path = SnapshotNode(
                    name=sub_path_parts[0],
                    is_dir=data.get("type") == "dir",
                    mode=data.get("mode"),
                    size=data.get("size", 0 if recursive else None),
                    gid=data.get("gid"),
                    uid=data.get("uid"),
                    atime=data.get("atime"),
                    mtime=data.get("mtime"),
                    ctime=data.get("ctime"),
                )
                nodes.append(base_path)

            # add subpath/file sizes to a path's size
            if recursive and len(sub_path_parts) > 1:
                base_path.size += data.get("size", 0)

        return nodes

    def get(self, file: str, zip_=False, **kwargs) -> IO:
        options = {}
        if zip_:
            options["archive"] = "zip"

        process = stream_restic_shell("dump", self.location, self.snapshot_id, file, **options)
        return process.stdout

    def restore(self, target, include: Optional[List[str]] = None, exclude: Optional[List[str]] = None):
        options = {}
        if include:
            options["include"] = include
        if exclude:
            options["exclude"] = exclude

        run_restic_shell("restore", self.location, self.snapshot_id, target=target, **options)

    @cached_property
    def cache_prefix(self) -> str:
        return f"restic_snapshots/{sha256(self.location.bucket_url.encode('utf-8')).hexdigest()}"

    @cached_property
    def cache_entry(self) -> str:
        return f"{self.cache_prefix}/{self.snapshot_id}"
