import json
import logging
import re
from typing import List, Optional

from backup_controller.backends.abstract import BackupBackend
from backup_controller.backends.restic.exceptions import ResticCommandError, BackupFailed
from backup_controller.backends.restic.snapshots import ResticSnapshotHandler
from backup_controller.backends.restic.utils import run_restic_shell
from backup_controller.models import Location

__all__ = ("ResticBackend",)

from backup_controller.models.objects import BackupSnapshot


class ResticBackend(BackupBackend):
    @staticmethod
    def _backup_location(paths: List[str], location: Location, exclude: Optional[List[str]]):
        try:
            run_restic_shell("snapshots", location)
        except ResticCommandError:
            run_restic_shell("init", location)

        kwargs = dict(ignore_inode=None, exclude=exclude or [])
        run_restic_shell("backup", location, *paths, **kwargs)

    def backup(self, paths: List[str], locations: List[Location], exclude_patterns: Optional[List[str]] = None):
        str_paths = list(str(p) for p in paths)

        logging.info(f"Backing up {' '.join(str_paths)} to {len(locations)} location(s)")
        errors = []

        for location in locations:
            logging.info(f"Backing up {' '.join(str_paths)} to {location.bucket_url}")
            try:
                self._backup_location(str_paths, location, exclude=exclude_patterns or [])
            except ResticCommandError as e:
                # check if snapshot has been saved anyway
                # this can happen if files get deleted during backup
                if e.exit_code == 3 and re.search(r"^snapshot \w+ saved$", e.stderr, flags=re.MULTILINE):
                    logging.debug(f"Some file(s) changed during snapshot saving.")
                    logging.info(f"Backup of {' '.join(str_paths)} to {location.bucket_url} completed")
                    continue
                logging.error(e)
                logging.error(f"Backup of {' '.join(str_paths)} to {location.bucket_url} failed")
                errors.append(f"Backup of {' '.join(str_paths)} to {location.bucket_url} failed")
            else:
                logging.info(f"Backup of {' '.join(str_paths)} to {location.bucket_url} completed")

        logging.info(f"Cleaning up {locations}")
        try:
            self.cleanup(locations)
        except ResticCommandError as e:
            logging.error(f"Could not cleanup old snapshots: {e}")

        if errors:
            raise BackupFailed("; ".join(errors))

    def cleanup(self, locations: List[Location]):
        for location in locations:
            run_restic_shell("unlock", location)
            retention = {k: v for k, v in location.retention_policy.dict().items() if v}
            run_restic_shell("forget", location, prune=None, **retention)

    def list(self, location: Location) -> List[BackupSnapshot]:
        output = run_restic_shell("snapshots", location, json=None)
        data = json.loads(output)
        return [BackupSnapshot(id=d["id"], time=d["time"], parent=d.get("parent"), paths=d["paths"]) for d in data]

    def snapshot(self, location, snapshot_id):
        return ResticSnapshotHandler(location, snapshot_id)
