import abc
from io import BytesIO
from pathlib import Path
from typing import List, Union, Optional, TYPE_CHECKING

if TYPE_CHECKING:
    from backup_controller.models import Location, BackupSnapshot
    from backup_controller.models.objects import SnapshotNode


class BackendException(Exception):
    """
    Base Exception for Backend Errors.
    """

    def get_reason(self):
        """
        Return reason of failure.
        """
        return "Unknown"


class SnapshotHandler(abc.ABC):
    def __init__(self, location: "Location", snapshot_id: str):
        self.location = location
        self.snapshot_id = snapshot_id

    @abc.abstractmethod
    def ls(self, path: str) -> List["SnapshotNode"]:
        """
        List files and directories of a snapshot's path

        :param path: Path to list
        :return: List of snapshot's path nodes (files and dirs)
        """

    @abc.abstractmethod
    def get(self, file: str, **kwargs) -> BytesIO:
        """
        Return file contents from the snapshot

        :param file: File path inside the snapshot
        :return: FP
        """

    @abc.abstractmethod
    def restore(self, target, include: Optional[List[str]] = None, exclude: Optional[List[str]] = None):
        """
        Return to target.

        :param target: Path to restore snapshot to
        :param include: List of files to restore
        :param exclude: List of files not to restore
        """


class BackupBackend(abc.ABC):
    """
    Abstract Backup Backend class.
    """

    @abc.abstractmethod
    def backup(
        self,
        paths: List[Union[str, Path]],
        locations: List["Location"],
        exclude_patterns: Optional[List[str]] = None,
    ):
        """
        Backup ``path`` to all ``locations``

        :param paths: Local Paths to back up
        :param locations: List of backup locations
        :param exclude_patterns: File/dir patterns to be excluded from the backup
        """

    @abc.abstractmethod
    def cleanup(self, locations: List["Location"]):
        """
        Cleanup ``locations`` according to their retention policy

        :param locations: List of backup locations
        """

    @abc.abstractmethod
    def list(self, location: "Location") -> List["BackupSnapshot"]:
        """
        List ``location`` snapshots

        :param location: Backup locations
        """

    @abc.abstractmethod
    def snapshot(self, location: "Location", snapshot_id: str):
        """Return SnapshotHandler for the requested snapshot"""
