import logging
from io import BytesIO
from pathlib import Path
from typing import List, Union, Optional

from backup_controller.backends.abstract import BackupBackend, SnapshotHandler
from backup_controller.models import Location, BackupSnapshot, SnapshotNode


class DummySnapshotHandler(SnapshotHandler):
    def ls(self) -> List[SnapshotNode]:
        return []

    def get(self, file: str, **kwargs) -> BytesIO:
        return BytesIO(b"")

    def restore(self, target, include: Optional[List[str]] = None, exclude: Optional[List[str]] = None):
        pass


class DummyBackend(BackupBackend):
    def cleanup(self, locations: List[Location]):
        pass

    def list(self, location: Location) -> List[BackupSnapshot]:
        return []

    def snapshot(self, location: Location, snapshot_id: str):
        return DummySnapshotHandler(location, snapshot_id)

    def backup(self, paths: List[Union[str, Path]], locations: List[Location]):
        logging.info(f"Dummy backup: {paths}")
