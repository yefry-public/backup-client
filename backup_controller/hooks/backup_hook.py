import uuid
from typing import Dict

from pydantic import BaseModel, Field

from backup_controller.hooks.utils import execute_hook


class BackupHook(BaseModel):
    id: str = Field(default_factory=uuid.uuid4)
    name: str
    description: str = ""
    script_path: str
    env: Dict[str, str] = Field(default_factory=dict)

    def execute(self):
        execute_hook(self.script_path)
