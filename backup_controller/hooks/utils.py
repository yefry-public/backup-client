import logging
import subprocess

from backup_controller.hooks.exceptions import HookCommandError


def execute_hook(cmd):
    logging.debug(f"Executing: {cmd}")
    process = subprocess.run(cmd, shell=True, env={}, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output = process.stdout.decode("utf-8")
    stderr = process.stderr.decode("utf-8")
    exit_code = process.returncode
    logging.debug(f"Command Output: [Exit {exit_code}] \nSTDOUT: {output}\nSTDERR: {stderr}")
    if exit_code != 0:
        raise HookCommandError(exit_code, stdout=output, stderr=stderr, cmd=cmd)
    return output
