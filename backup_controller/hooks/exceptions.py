from backup_controller.exceptions import CommandError


class HookException(Exception):
    """
    Base Exception for Hook errors.
    """


class HookCommandError(CommandError, HookException):
    """
    Hook command execution error.
    """
