#!/bin/bash
RE_EXCLUDE='^sys$|^information_schema$|^performance_schema$|^mysql$'

# Load environment variables from .env.mysqldump file
if [ -f .env.mysqldump ]; then
  source .env.mysqldump
else
  echo "Error: .env.mysqldump file not found"
  exit 1
fi

function get_databases() {
  mysql -N -e 'show databases' -h$1 -u$2 -p$3 | grep -Ev $RE_EXCLUDE
}

function backup_database() {
  echo "Backing up database $4"
  mysqldump --single-transaction -h$1 -u$2 -p$3 "$4" | gzip > $5/"$4".sql.gz
}

function backup_all_databases() {
  echo "Clearing directory $4"
  rm -rf $4
  mkdir -p $4
  get_databases $1 $2 $3 | while read dbname; do backup_database $1 $2 $3 $dbname $4; done
}

backup_all_databases $HOST $USER $PASS $DIR
