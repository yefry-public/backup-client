from backup_controller.hooks.backup_hook import BackupHook
from backup_controller.conf.base import *

# Create an instance of BackupHook with the provided data
default_hook_001 = BackupHook(
    id="mysqldump",
    description="",
    name="Dump MySQL databases",
    script_path=str(CONFIG_DIR / "hooks" / "dump_databases.sh"),
)

# Add the created instance to the default hooks list
default_hooks = [default_hook_001]