import logging
import sys

from .cli import main

if __name__ == "__main__":
    from .conf import setup_logging, setup_signals
    from .utils import terminate

    setup_logging()
    setup_signals()

    try:
        terminate(main())
    except Exception as e:
        logging.critical(e, exc_info=True)
        terminate(255)
